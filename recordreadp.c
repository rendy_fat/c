#include <stdio.h>
#include <string.h>

typedef struct{ //data apa saja yg akan dimasukan ke dalam record
	char nim[10];
	char nama[50];
	char nilai[5];
}nilaimatkul;

int main(){
	nilaimatkul record;
	FILE *datamatkul;
	datamatkul=fopen("Datamatkul.be", "r");
	char cari[10];
	printf("masukan nim yang dicari\n");
	scanf("%s", &cari);
	fscanf(datamatkul, "%s %s %s\n", &record.nim, &record.nama, &record.nilai);
	
	if(strcmp(record.nim, "XXXXXXX") ==0){
		printf("data kosong\n");
	}
	else{
		if(strcmp(cari, "XXXXXXX") ==0){
			printf("tidak ditemukan\n");
		}
		else{
			while((strcmp(record.nim, "XXXXXXX") !=0) && (strcmp(record.nim, cari) !=0)){
				fscanf(datamatkul, "%s %s %s\n", &record.nim, &record.nama, &record.nilai);
			}
		}
		if(strcmp(record.nim, cari)==0){
			//Proses
			printf("Hasil pencarian : \n");
			printf("nim : %s\n", record.nim);
			printf("nama : %s\n", record.nama);
			printf("nilai : %s\n", record.nilai);
			printf("-----------------\n");
		}
		else{
			printf("tidak ditemukan \n");
		}
	}
	
	fclose(datamatkul);
	
	return 0;
}