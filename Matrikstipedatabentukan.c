#include <stdio.h>
typedef struct{
	int x; //horizontal
	int y; //vertikal
}koordinat;

int main(){
	koordinat titik[3][3]; //deklarasi variabel titik yang didalamnya ada koordinat x dan yang
	int i,j;
	
	for(i=0 ; i<3 ; i++){
		for(j=0 ; j<3 ; j++){
			printf("Untuk elemen baris %d kolom %d\n", i , j);
			printf("Masukan koordinat x : ");
			scanf("%d", &titik[i][j].x);
			printf("Masukan koordinat y : ");
			scanf("%d", &titik[i][j].y);
		}
	}
	
	for(i=0 ; i<3 ; i++){
		for(j=0 ; j<3 ; j++){
			printf("Elemen pada baris ke %d dan kolom ke %d\n", i+1, j+1);
			printf("koordinat x : %d", titik[i][j].x);
			printf("koordinat y : %d\n", titik[i][j].y);
		}
	}
	return 0;
}