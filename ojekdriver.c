#include <stdio.h>
#include <string.h>

typedef struct{
	char nama[100];
	char no_telepon[100];
	int jam_terbang;
	double komisi;
}driver;

int main(){
	int n,m,p; //untuk menentukan variable banyaknya elemen namaay 1,2 dan 3.
	int i,j;
	char pilih[50];
	scanf("%s", &pilih);
	scanf("%d", &n);//input banyaknya
	driver ojek1[n];//ojek driver 2014
	for(i=0 ; i<n ; i++){
		scanf("%s %s %d %lf", &ojek1[i].nama, &ojek1[i].no_telepon, &ojek1[i].jam_terbang, &ojek1[i].komisi);
	}
	
	scanf("%d", &m);//input banyaknya
	driver ojek2[m];// ojek driver tahun 2015
	for(i=0 ; i<m ; i++){
		scanf("%s %s %d %lf", &ojek2[i].nama, &ojek2[i].no_telepon, &ojek2[i].jam_terbang, &ojek2[i].komisi);
	}
	
	scanf("%d", &p);//input banyaknya
	driver ojek3[p]; //ojek driver tahun 2016
	for(i=0 ; i<p ; i++){
		scanf("%s %s %d %lf", &ojek3[i].nama, &ojek3[i].no_telepon, &ojek3[i].jam_terbang, &ojek3[i].komisi);
	}
	if(strcmp(pilih,"insert")==0){
		insert(n,ojek1);
		insert(m,ojek2);
		insert(p,ojek3);
	}
		// if(strcmp(pilih,"quick")==0){
		// quick();
	// }
		if(strcmp(pilih,"bubble")==0){
		bubble(n,ojek1);
		bubble(m,ojek2);
		bubble(p,ojek3);
	}
		if(strcmp(pilih,"selection")==0){
		select(n,ojek1);
		select(m,ojek2);
		select(p,ojek3);
	}
	
	driver hasil1[m+n];
    driver hasil2[m+n+p];

    merge(m, n, ojek1, ojek2, hasil1);
    merge(m+n, p, hasil1, ojek3, hasil2);

    for (i = 0; i < m+n+p; i++) {
        printf("%s %s %d %lf\n", hasil2[i].nama, hasil2[i].no_telepon, hasil2[i].jam_terbang, hasil2[i].komisi);
    }
	return 0;
}

void bubble(int n, driver ojek[]){
	char tempnama[50], temptel[50];
	int tempjam, tempkom;
	int i;
	int tukar;
	do{
        tukar = 0;
        for(i = 0; i < (n-1); i++){

            if(ojek[i].komisi < ojek[i+1].komisi){
                tempkom= ojek[i].komisi;
                ojek[i].komisi = ojek[i+1].komisi;
                ojek[i+1].komisi = tempkom;
                
				tempjam= ojek[i].jam_terbang;
                ojek[i].jam_terbang = ojek[i+1].jam_terbang;
                ojek[i+1].jam_terbang = tempjam;
				
				strcpy(tempnama, ojek[i].nama);
				strcpy(ojek[i].nama, ojek[i+1].nama);
				strcpy(ojek[i+1].nama, tempnama);
				
				strcpy(temptel, ojek[i].no_telepon);
				strcpy(ojek[i].no_telepon, ojek[i+1].no_telepon);
				strcpy(ojek[i+1].no_telepon, temptel);
				
				tukar = 1;
            }

        }
    } while(tukar == 1);
} 

void select(int n, driver ojek[]){
	char tempnama[50], temptel[50];
	int tempjam, tempkom;
	
	int i, j;
    int m;

    for (i = 0; i < n-1; i++) {
        m = i; 
        for (j = i+1; j < n; j++) {
            if(ojek[m].komisi < ojek[j].komisi) {
                m = j; 
            }
        }
        
        tempkom = ojek[i].komisi;
        ojek[i].komisi = ojek[m].komisi;
        ojek[m].komisi = tempkom;
		
		tempjam= ojek[i].jam_terbang;
        ojek[i].jam_terbang = ojek[m].jam_terbang;
        ojek[i+1].jam_terbang = tempjam;
				
		strcpy(tempnama, ojek[i].nama);
		strcpy(ojek[i].nama, ojek[m].nama);
		strcpy(ojek[m].nama, tempnama);
				
		strcpy(temptel, ojek[i].no_telepon);
		strcpy(ojek[i].no_telepon, ojek[m].no_telepon);
		strcpy(ojek[m].no_telepon, temptel);
    }
}

void insert(int n, driver ojek[]){
	char tempnama[50], temptel[50];
	int tempjam, tempkom;
	int i,j,m;
	
	for(i=0; i<n ; i++){
		m=ojek[i].jam_terbang;
		m=ojek[i].komisi;
		strcpy(tempnama, ojek[i].nama);
		strcpy(temptel, ojek[i].no_telepon);
		j=i-1;
		while((m > ojek[j].komisi) && (j>=0)){
			ojek[j+1].komisi=ojek[j].komisi;
			ojek[j+1].jam_terbang=ojek[i].jam_terbang;
			strcpy(ojek[j+1].nama,ojek[j].nama);
			strcpy(ojek[j+1].no_telepon,ojek[j].no_telepon);
			j=j-1;
		}
		ojek[j+1].jam_terbang=m;
		ojek[j+1].komisi=m;
		strcpy(ojek[j+1].nama, tempnama);
		strcpy(ojek[j+1].no_telepon, temptel);
	}
	for (i = 0; i < n; i++){
		for (j = 0; j < n; j++){
			if((strcmp(ojek[j].nama, ojek[i].nama)==-1)){
				m=j;
			}
		}
		strcpy(tempnama, ojek[i].nama);
		strcpy(ojek[i].nama, ojek[m].nama);
		strcpy(ojek[m].nama, tempnama);
		strcpy(temptel, ojek[i].no_telepon);
		strcpy(ojek[i].no_telepon, ojek[m].no_telepon);
		strcpy(ojek[m].no_telepon,temptel);
		tempkom=ojek[i].komisi;
		ojek[i].komisi=ojek[m].komisi;
		ojek[m].komisi=tempkom;
		tempjam=ojek[i].komisi;
		ojek[i].jam_terbang=ojek[m].jam_terbang;
		ojek[m].jam_terbang=tempjam;
		}
	
}

// void quick(int n, driver ojek[], int l, int r){
	// char tempnama[50], temptel[50];
	// int tempjam, tempkom;
	// int i,j;
// }

void merge(int n, int m, driver ojek1[],driver ojek2[], driver hasil[]){
	int i;
	
	int hitung1=0; int hitung2=0; int hitung3=0;
	 
	 while((hitung1 < m) && (hitung2 < n)){
        if(ojek1[hitung1].komisi > ojek2[hitung2].komisi){
            hasil[hitung3] = ojek1[hitung1];

            hitung1++;
            hitung3++;
        }else if(ojek2[hitung2].komisi > ojek1[hitung1].komisi){
            hasil[hitung3] = ojek2[hitung2];

            hitung2++;
            hitung3++;
        }else{
            hasil[hitung3] = ojek1[hitung1];
            hitung1++;
            hitung3++;

            hasil[hitung3] = ojek2[hitung2];
            hitung2++;
            hitung3++;
        }
    }
    if(hitung1 < m){
        for(i = hitung1; i < m; i++){
            hasil[hitung3] = ojek1[i];
            hitung3++;
        }
    }
    if(hitung2 < n){
        for(i = hitung2; i < n; i++){
            hasil[hitung3] = ojek2[i];
            hitung3++;
        }
    }
	
}