#include <stdio.h>

typedef struct {
	int harga, banyak;
} makanan;
/*kita sudah membuat sebuah bungkusan yang terdiri
dari 2 kotak dengan tipe yang sama*/

int main (){
	
	makanan nasi_goreng, capcay; //makanan berfungsi sebagai tipe data
	//nasi_goreng dan capcay berfungsi sebagai variabel
	
	nasi_goreng.harga = 10000; //inisialisasi harga
	capcay.harga = 4000;
	
	printf("masukan jumlah nasi goreng. \n");
	scanf("%d", &nasi_goreng.banyak);
	printf("masukan jumlah capcay. \n");
	scanf("%d", &capcay.banyak);
	
	printf("total harga nasi goreng : %d\n", nasi_goreng.harga*nasi_goreng.banyak);
	printf("total harga capcay : %d\n", capcay.harga*capcay.banyak);
	
	return 0;
	
}
