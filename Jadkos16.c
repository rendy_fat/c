#include <stdio.h>
#include <string.h>

typedef struct{
	int hari; //variabel hari
	int jmulai; // jam mulai pelajaran
	int jselesai; //jam berakhir pelajaran
}kelas;

int main(){
	int n,m,o; //variabel banyak data
	int i; //variabel looping
	
	scanf("%d", &n); //banyak data tobi
	kelas tobi[n];
	for(i=0 ; i<n ; i++){
		scanf("%d %d %d", &tobi[i].hari, &tobi[i].jmulai, &tobi[i].jselesai);
	}
	
	scanf("%d", &m); //banyak data moni
	kelas moni[m];
	for(i=0 ; i<m ; i++){
		scanf("%d %d %d", &moni[i].hari, &moni[i].jmulai, &moni[i].jselesai);
	}
	
	scanf("%d", &o); //banyak data piyu
	kelas piyu[o];
	for(i=0 ; i<o ; i++){
		scanf("%d %d %d", &piyu[i].hari, &piyu[i].jmulai, &piyu[i].jselesai);
	}
	
	
	
	return 0;
}

void hasil(int x, kelas data[x]){
	char nhari[x][10];
	int i;
	
	for(i=0 ; i<k ; i++){
		if(data[i].hari==1){
			strcpy(nhari[i],"Senin");
		}
		if(data[i].hari==2){
			strcpy(nhari[i],"Selasa");
		}
		if(data[i].hari==3){
			strcpy(nhari[i],"Rabu");
		}
		if(data[i].hari==4){
			strcpy(nhari[i],"Kamis");
		}
		if(data[i].hari==5){
			strcpy(nhari[i],"Jumat");
		}
	}
	if(x != 0){
		for(i=0 ; i<x ; i++){
			printf("%s %d.00 - %d.00\n", nhari[i], data[i].jmulai, data[i].jselesai);
		}
	}
	else{
		printf("Tidak ada irisan jadwal.\n");
	}
}

void search(int waktu[10][30], int n, int m, int o){
	kelas data[50]; //temporari untuk irisan jadwal kosong
	int setwaktu;
	int i, j=0, k;
	int p=0; //counter dari waktu kosong
	
	for(i=1; i<=5 ; i++){
		setwaktu=0;
		for(j=7; j<18 ; i++){
			if(waktu[i][j]==0){
				data[p].hari=i;
				data[p].jmulai=j;
				k=j;
				
				while(j<=18 && setwaktu==0){
					if(waktu[i][j]==1 || j==18){
						setwaktu=1;
						data[p].jselesai=j;
					}
					else{j++}
				}
				p++;
			}
		}
	}
	hasil(x, data);
}

void ngatandaan(int n, int m, int o, kelas tobi[], kelas moni[], kelas piyu[]){
	int tanda[10][30]={0}; //penanda jadwal
	int i,j; //variabel looping
	
	for(i=0 ; i<n ; i++){
		for(j=tobi[i].jmulai ; j<tobi[i].jselesai ; j++){
			tanda[tobi[i].hari][j]=1;
		}
	}
	
	for(i=0 ; i<m ; i++){
		for(j=moni[i].jmulai ; j<moni[i].jselesai ; j++){
			tanda[moni[i].hari][j]=1;
		}
	}
	
	for(i=0 ; i<o ; i++){
		for(j=piyu[i].jmulai ; j<piyu[i].jselesai ; j++){
			tanda[piyu[i].hari][j]=1;
		}
	}
	search(waktu, n, m, o);
} 



/*
Contoh Masukan

5
1 7 18
2 7 10
2 16 18
3 7 12
4 9 12

3
2 7 18
3 10 16
4 7 16

4
1 8 12
1 16 17
3 8 12
4 7 10

Contoh Keluaran

Rabu 16.00 - 18.00
Kamis 16.00 - 18.00
Jumat 7.00 - 18.00
Contoh Masukan 2

5
1 7 18
2 7 16
3 9 16
4 7 12
4 16 18

4
2 10 18
3 7 10
4 7 16
5 10 18

3
3 16 18
5 7 10
5 13 16

Contoh Keluaran 2

Tidak ada irisan jadwal.
       




*/