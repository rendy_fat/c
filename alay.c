#include <stdio.h>
#include <string.h>

int main(){
	int n,i,j,find,total; //banyak string dan counter dan status dan total jumlah kata alay
	scanf("%d", &n);//masukan jumlah string
	
	char arrs[n][50]; //array of string
	//proses masukan
	for(i=0; i<n; i++){
		scanf("%s", &arrs[i]);
	}
	total = 0;//total kata alay diinisialisasi
	
	//proses penghitungan kata alay dalam array of string
	for(i=0; i<n; i++){
		j=0;
		find=0;
		while(j<strlen(arrs[i]) && find==0){
			if((arrs[i][j]=='1') || (arrs[i][j]=='2') ||
			(arrs[i][j]=='3') || (arrs[i][j]=='4') ||
			(arrs[i][j]=='5') || (arrs[i][j]=='6') ||
			(arrs[i][j]=='7') || (arrs[i][j]=='8') ||
			(arrs[i][j]=='9') || (arrs[i][j]=='0')){
				find=1;
				//jumlah total bertambah
				total=total+1;
			}
			else{
				//iterasi
				j=j+1;
			}
		}
	}
	printf("%d\n", total);
	return 0;
}