#include <stdio.h>

int main (){
	int ketebalan, baris, kolom;
	
	scanf("%d", &ketebalan);
	
	for(baris=0; baris<ketebalan; baris++){
		for(kolom=0; kolom<=baris; kolom++){
			printf("*");
		}
	
		printf("\n");
	}
	for(baris=0; baris<ketebalan; baris++){
		for(kolom=ketebalan; kolom>baris + 1; kolom++){
			printf("*");
		}
	
		printf("\n");
	}
	return 0;
}