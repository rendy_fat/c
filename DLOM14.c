#include <stdio.h>
#include <string.h>

int main(){
  	int matriks[10][10];
	int b, k, i, j;
	int x, y;
	for(b=0 ; b<10; b++){
		for(k=0 ; k<10 ; k++){
			matriks[b][k]=0;
		}
	}
	scanf("%d %d", &x, &y);
	x=x-1; y=y-1;
	matriks[x][y]=3;
	int n,a;
	char s[10];
	scanf("%d", &n);
	for(i=0 ; i<n ; i++){
		scanf("%s %d", &s, &a);
		if(strcmp(s, "kanan")==0){
			for(b=0 ; b<a ; b++){
				matriks[x+b+1][y]=2;
			}
			x=x+a;
		}
		if(strcmp(s, "atas")==0){
			for(b=0 ; b<a ; b++){
				matriks[x][y-b-1]=1;
			}
			y=y-a;
		}
		if(strcmp(s, "bawah")==0){
			for(b=0 ; b<a ; b++){
				matriks[x][y+b+1]=1;
			}
			y=y+a;
		}
		if(strcmp(s, "kiri")==0){
			for(b=0 ; b<a ; b++){
				matriks[x-b-1][y]=2;
			}
			x=x-a;
		}
	}
	
	
	for(k=0 ; k<10 ; k++){
		for(b=0; b<10 ; b++){
			if(matriks[b][k] == 0){
				printf("*");
			}
			else if(matriks[b][k] == 1){
				printf("|");
			}
			else if(matriks[b][k] == 2){
				printf("-");
			}
			else if(matriks[b][k] == 3){
				printf("x");
			}
		}
		printf("\n");
	}
	
	return 0;
}