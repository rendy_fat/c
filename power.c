#include <stdio.h>

int power (int x, int y){
	int hasil;
	
	if(y==0){
		return 1;
	}
	else{
		hasil=x*power(x, y-1);
		return hasil;
	}
}

int main(){
	int bil, pangkat;
	scanf("%d %d", &bil, &pangkat);
	int hasil = power(bil, pangkat);
	printf("hasil pangkat: %d\n", hasil);
	return 0;
}