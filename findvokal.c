#include <stdio.h>
#include <string.h>

int main(){
	int n,i,j, h=0;
	scanf("%d", &n);
	char arr[n][50];
	for(i=0;i<n;i++){
		scanf("%s", &arr[i]);
	}
	for(i=0;i<n;i++){
		for(j=0;j<strlen(arr[i]);j++){
			if((arr[i][j] =='a') || (arr[i][j] =='i') || (arr[i][j] =='u') || (arr[i][j] =='e') || (arr[i][j] =='o')){
				h++;
			}
		}
	}
	printf("%d", h);
	return 0;
}