#include <stdio.h>

int rekursif(int r1, int r2, int r3, int ngurut){ //fungsi untuk melakukan fibbonacci
	 int aritmatik=r1+r2;// variabel penambahan
	 int sement; //tempat sementara
	 
	 if(r1==r3){
		 return 1; // basis
	 }
	 else if(r2==r3){
		 return 2; //basis
	 }
	 else if(aritmatik==r3){
		 return ngurut; // untuk urutan fibbonacci
	 }
	 else{
		 sement=r2; //sementara = r2
		 r2=aritmatik; // r2 = aritmatik
		 r1=sement; //r1 = sementara
		 ngurut++; // urutan bertambah
		 return rekursif(r1, r2, r3, ngurut); //proses diulangi
	 }
}
int main(){
	int x,y; //variabel masukan untuk matriks;
	scanf("%d %d", &x, &y); // inputan untuk matriks
	int i,j; // variabel looping
	int r1[x][y]; //basis 1
	int r2[x][y]; //basis 2
	int r3[x][y]; //deret fibbonacci
	int r4[x][y]; //parameter deret
	char r5[x][y]; //nilai bandingan = outputnya
	int ngurut =3; //inisialisasi
	
	for(i=0; i<x ;i++){
		for(j=0 ; j<y ; j++){
			scanf("%d", &r1[x][y]); //angka masukan matriks 1 = basis
		}
	}
	for(i=0; i<x ;i++){
		for(j=0 ; j<y ; j++){
			scanf("%d", &r2[x][y]); //angka masukan matriks 2 = basis
		}
	}
	for(i=0; i<x ;i++){
		for(j=0 ; j<y ; j++){
			scanf("%d", &r3[x][y]); //angka masukan matriks 3 = fibbonacci
		}
	}
	for(i=0; i<x ;i++){
		for(j=0 ; j<y ; j++){
			scanf("%d", &r4[x][y]); //angka masukan matriks 4 = parameter deret
		}
	}
	
	for(i=0 ; i<x ; i++){
		for(j =0 ; j<y ; j++){
			if(r4[i][j]>= rekursif(r1[i][j], r2[i][j], r3[i][j], ngurut)){
				r5[i][j]='X';
			}
			if(r4[i][j]<= rekursif(r1[i][j], r2[i][j], r3[i][j], ngurut)){
				r5[i][j]='O';
			}
		}
	}
	
	for (i = 0; i < x; i++){
		for (j = 0; j < y; j++){
			printf("%c ", r5[i][j]);	
		}
		printf("\n");
	}
	return 0;
}