#include <string.h>
#include <stdio.h>

typedef struct{
	char nim[10];
	char nama[100];
	float nilai;
}nmin;

nmin tabel[5];

void isi(int i, char nim[], char nama[], float nilai){
	strcpy(tabel[i].nama, nama);
	strcpy(tabel[i].nim, nim);
	tabel[i].nilai=nilai;
}

int main(){
	int ketemu;
	int i;
	
	char nim_mari[50];
	
	isi(0,"1501349","Rendy Ganteng",67.55);
	isi(1,"1601349","Rendy Kazep",87.21);
	isi(2,"1701349","Boa Rendy",78.98);
	isi(3,"1801349","Edan Rendy",77.77);
	isi(4,"1901349","Mantap Rendy",66.66);
	strcpy(nim_mari,"1501349");
	ketemu = 0;
	i=0;
	while((i<5) && (ketemu == 0)){
		if(strcmp(tabel[i].nim, nim_mari) ==0){
			ketemu=1;
		}
		else{
			i++;
		}
	}
	if(ketemu==1){
		printf("nim %s\n", tabel[i].nim);
		printf("nama %s\n", tabel[i].nama);
		printf("nilai %0.2f\n", tabel[i].nilai);
	}
	else{
		printf("tidak ditemukan");
	}
	return 0;
}

