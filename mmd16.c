#include <stdio.h>
#include <string.h>

void bsortnim(int n, char nim[n][50], char nama[n][50]); //bahan untuk mesin
void qsortnama(int n, char nim[n][50], char nama[n][50], int l, int r); //bahan untuk mesin
void cek(int n, char nama[n][50], char nim[n][50], char pesan[10], char pilih[10]); //bahan untuk mesin

int main(){
	int n; //variabel untuk banyaknya data yg digunakan
	scanf("%d", &n);//input data banyaknya
	char pesan[10];//variabel untuk menanyakan ya\tidak\terserah
	scanf("%s", &pesan);//input data pesan
	int i;
	char nama[n][50], nim[n][50];//variabel untuk memasukan nama dan nim
	for(i=0 ; i<n ; i++){
		printf("%s %s", &nim[i], &nama[i]); //input nim dan nama
	}
	char pilih[10]; //char untuk memilih yang ingin di sort
	scanf("%s", &pilih);// input nama atau nim
	
	if(strcmp(pilih,"nama")==0 || strcmp(pilih,"nim")==0){
		cek(n,nim,nama,pesan,pilih); //memanggil void untuk memeriksa
	}
	else{
		printf("---Data tidak dapat diproses---");
	}
	return 0;
}

void bsortnim(int n, char nim[n][50], char nama[n][50]){ //void untuk mensortir nim menggunakan bubble
	int tukar; //variabel untuk melakukan pertukaran
	int i; //variabel looping
	char tempnama[50]; //variabel untuk menyimpan nama sementara
	char tempnim[50]; //variabel untuk menyimpan nim sementara
	tukar =0; //inisialisasi nilai tukar
	//Proses pertukaran
	do{
		for(i=0 ; i<(n-1) ; i++){
			if(strcmp(nim[i], nim[i+1])==1){
			strcpy(tempnama, nama[i]);
			strcpy(nama[i], nama[i+1]);
			strcpy(nama[i+1], tempnama);
			
			strcpy(tempnim, nim[i]);
			strcpy(nim[i], nim[i+1]);
			strcpy(nim[i+1], tempnim);
			
			tukar = 1;
			}
		}
	}
	while(tukar==1);
}

void qsortnama(int n, char nim[n][50], char nama[n][50], int l, int r){ //void untuk mensortir nama dengan menggunakan quick sort
	int i, j; //variabel mencari batas
	char temp[50]; //variabel temporary
	i=l, j=r; //variabel agar i dan j tidak melebihi batas
	//proses sortir
	do{
		while(strcmp(nama[i], nama[r])==-1 && (i<=j)){
			i++;
		}
		while(strcmp(nama[j], nama[l])==1 && (i<=j)){
			j--;
		}
		if(i<j){
			strcpy(temp, nama[i]);
			strcpy(nama[i], nama[j]);
			strcpy(nama[j], temp);
			
			strcpy(temp, nim[i]);
			strcpy(nim[i], nim[j]);
			strcpy(nim[j], temp);
			i++;
			j--;
		}
	}
	while(i<j);
	
	//rekursif
	if(l<j){
		qsortnama(n,nim,nama,l,j);
	}
	if(i<r){
		qsortnama(n,nim,nama,i,r);
	}
}

void cek(int n, char nama[n][50], char nim[n][50], char pesan[10], char pilih[10]){ //void untuk menampilkan proses akhir
	char nimerror[20], namaerror[100]; //variabel untuk nama dan nim yang error
	int i,x,z,a; //variable untuk looping
	int j=0; 
	for(i=0 ; i<n ; i++){
		//proese  sorting
		if(strlen(nim[i])==7){
			for(x=0 ; x<strlen(nim[i]) ; x++){
				if(nim[i][x]!='1' && nim[i][x]!='2' && nim[i][x]!='3' && nim[i][x]!='4' && nim[i][x]!='5' && nim[i][x]!='6' && nim[i][x]!='7' && nim[i][x]!='8' && nim[i][x]!='9' && nim[i][x]!='0'){ //algoritma untuk memasukan nim ke dalam daftar error xika ada huruf di nim selain angka
						x=strlen(nim[i]);
						strcpy(namaerror[j], nama[i]);
						strcpy(nimerror[j], nim[i]);
						j++;
						for (z = i; z < n-1; z++){
							strcpy(nim[z], nim[z+1]);
							strcpy(nama[z], nama[z+1]);
						}
					if(i>0){
						i--; n--;
					}
				}
			}
		}
		else if(strlen(nim[i])!=7){ //algoritma untuk memasukan ke daftar error apabila panxang huruf tidak 7
			for(x=0 ; x<strlen(nim[i]) ; x++){
				if(nim[i][x]!='1' && nim[i][x]!='2' && nim[i][x]!='3' && nim[i][x]!='4' && nim[i][x]!='5' && nim[i][x]!='6' && nim[i][x]!='7' && nim[i][x]!='8' && nim[i][x]!='9' && nim[i][x]!='0'){ //algoritma untuk memasukan nim ke dalam daftar error jika ada huruf di nim selain angka
							x=strlen(nim[i]);
							strcpy(namaerror[j], nama[i]);
							strcpy(nimerror[j], nim[i]);
							j++;
							for (z = i; z < n-1; z++){
								strcpy(nim[z], nim[z+1]);
								strcpy(nama[z], nama[z+1]);
							}
					if(i>0){
						i--; n--;
					}
				}
			} 
		}
	}
	if(n>0){// xika ada data valid maka akan di sortir
		if(strcmp(pilih,"nama")==0){
			qsortnama(n,nim,nama,0,n-1); //memanggil void untuk mensort nama
		}
		if(strcmp(pilih,"nim")==0){
			bsortnim(n,nim,nama); //memanggil void untuk menyortir nim
		}
		printf("---Hasil Pengurutan---\n");
		for (i = 0; i < n; ++i){
			printf("%s %s\n", nim[i], nama[i] );
		}
	}
	else{
		printf("Tidak ada data valid.\n");
	}
	
	if(strcmp(pesan,"ya")==0){
		printf("---Daftar Error---\n");
		for(i=0 ; i<j ; i++){
			printf("%s - %s\n", nimerror[i], namaerror[i]);
		}
	}
}

// Buatlah mesin pengurut daftar mahasiswa dengan data berupa NIM dan mahasiswa dengan ketentuan sebagai berikut :

// Input

// N (banyaknya data yang akan digunakan)
// ya/tidak 
// Menampilkan pesan error atau tidak, jika input bukan keduanya, pesan error tidak ditampilkan.
// NIM & NAMA sebanyak N
// NIM harus semuanya berupa angka dan memiliki 7 karakter.
// Jika NIM tidak sesuai, tambahkan ke dalam daftar error.
// nim/nama 
// Data yang akan diurutkan, nim dengan bubble, nama dengan quick.
// Jika bukan keduanya, hentikan program dengan pesan berupa "---Data tidak dapat diproses---".
// Output

// ---Hasil Pengurutan---
// [Data yang sudah diurutkan] (Jika ada) / "Tidak ada data valid." (Jika tidak ada data valid)
// ---Daftar Error--- (Jika ada error & ditampilkan)
// [Pesan data yang bermasalah sesuai format] (Jika ada error & ditampilkan)

// Contoh Masukan

// 7
// terserah
// 124j421 EzBugatti
// 2000001 LienKing
// 0800322 Artizi
// 1520036 Yunifers
// 322322322 Ice3
// 4429222 Puppan
// 7732221 EternalEnpi
// nim

// Contoh Keluaran

// ---Hasil Pengurutan---
// 0800322 Artizi
// 1520036 Yunifers
// 2000001 LienKing
// 4429222 Puppan
// 7732221 EternalEnpi

// Contoh Masukan 2

// 3
// ya
// 124j421 EzBugatti
// 322322322 Ice3
// 44292223 Puppan
// nama

// Contoh Keluaran 2

// ---Hasil Pengurutan---
// Tidak ada data valid.
// ---Daftar Error---
// 124j421 - EzBugatti tidak valid.
// 322322322 - Ice3 tidak valid.
// 44292223 - Puppan tidak valid.
       
// Contoh Masukan 3

// 7
// ya
// 124j421 SadLife
// 0241281 Cincong
// 2332111 Cadas
// 12894121 Artizi
// 4429222 Puppan
// 7732221 EternalEnpi
// 9321123 Yunifers
// mmr

// Contoh Keluaran 3

// ---Data tidak dapat diproses---
