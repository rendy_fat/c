#include <stdio.h>

int main(){
	int tabint[10];
	int i, temp, minindeks,j;
	for(i=0 ; i<10 ; i++){
		scanf("%d", &tabint[i]);
	}
	for(i=0 ; i<(10-1); i++){
		minindeks =1;
		for(j=(i+1); j<10 ; j++){
			if(tabint[minindeks] > tabint[j]){
				minindeks=j;
			}
		}
		temp=tabint[i];
		tabint[i]=tabint[minindeks];
		tabint[minindeks]=temp;
	}
	
	for(i=0 ; i<10 ; i++){
		printf("%d\n", tabint[i]);
	}
	
	return 0;
}